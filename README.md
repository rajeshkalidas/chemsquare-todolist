# chemsquare-todolist

About Application  :-

This Application provides TodoListItem in ascending order.
User can have to add, retrive and delete todos


Project Technical Descriptions :-
This Project is created and involved the technologies like
React JS, Material-UI
Node JS and Express JS
SQLite


Intructions to Run application :-
1. Navigate to project folder from git bash/command prompt.
2. Type "npm run getDependencies" and then "npm run dev".
3. To start server only, execute "npm run server"
4. To start client only, execute "npm run client"
5. to start test cases, execute "npm run test"
